import ru.entity.Fork;
import ru.entity.Philosopher;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Application {
    public static void main(String[] arg) throws InterruptedException {
        Fork fork1 = new Fork("fork 1");
        Fork fork2 = new Fork("fork 2");
        Fork fork3 = new Fork("fork 3");
        Fork fork4 = new Fork("fork 4");
        Fork fork5 = new Fork("fork 5");
        ExecutorService list = Executors.newFixedThreadPool(5);

        list.submit(new Philosopher(fork5,fork1, "Philosopher 1"));
        list.submit(new Philosopher(fork1,fork2, "Philosopher 2"));
        list.submit(new Philosopher(fork2,fork3, "Philosopher 3"));
        list.submit(new Philosopher(fork3,fork4, "Philosopher 4"));
        list.submit(new Philosopher(fork4,fork5, "Philosopher 5"));

        list.shutdown();

        list.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
    }
}
