package ru.entity;

import java.util.Random;

public class Philosopher implements Runnable {
    private static final int EATING_ITERATION = 5;
    private Random random = new Random();
    private Fork rightFork;
    private boolean leftForkOccupaed = false;
    private boolean rightForkOccupaed = false;
    private Fork leftFork;
    private String name;

    public Philosopher(Fork rightFork, Fork leftFork, String name) {
        this.rightFork = rightFork;
        this.leftFork = leftFork;
        this.name = name;
    }

    @Override
    public void run() {
        for (int i  = 0; i < EATING_ITERATION;  i++){
            eating();
            thinking();
        }
    }

    private void eating(){
        leftForkOccupaed = leftFork.occupy(name);
        rightForkOccupaed = rightFork.occupy(name);
        if(leftForkOccupaed && rightForkOccupaed) {
            try {
                System.out.println(name + " is eating");
                Thread.sleep(random.nextInt(1000) + 1000);
            } catch (InterruptedException e) {
                System.out.println("houston we have a problem");
            }
        }
    }

    private void thinking(){
        if (leftForkOccupaed) {
            leftFork.release(name);
            leftForkOccupaed = false;
        }
        if (rightForkOccupaed){
            rightFork.release(name);
            rightForkOccupaed = false;
        }

        try{
            System.out.println(name + " is thinking");
            Thread.sleep(random.nextInt(1000) + 1000);
        } catch (InterruptedException e) {
            System.out.println("houston we have a problem");
        }
    }


}
