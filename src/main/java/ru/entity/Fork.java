package ru.entity;

import java.util.Random;

public class Fork {
    private boolean used = false;
    private String name;
    private Random random = new Random();

    public Fork(String name){
        this.name = name;
    }

    public synchronized boolean occupy(String byWho){
        try {
            if (used) {
                wait(1000 + random.nextInt(500));
            }
        }catch (Exception e){
            System.out.println("houston we have a problem");
        }
        if (used) {
            System.out.println(byWho + ": screw you guys i'm going home");
            notify();
            return false;
        }
        used = true;
        System.out.println(name + " occupied by " + byWho);
        notify();
        return true;
    }

    public synchronized void release(String byWho){
        try {
            while (!used) {
                wait();
            }
        }catch (Exception e){
            System.out.println("houston we have a problem");
        }
        used = false;
        System.out.println(name + " released by " + byWho);
        notify();
    }

}
